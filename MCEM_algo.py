#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Copyright (c) 2019 by Inria
Authored by Simon Leglaive (simon.leglaive@inria.fr)
License agreement in LICENSE.txt
"""

import numpy as np
from utils import ARE_solver, multi_mult, multi_det, multi_trace, multi_inv

class MCEM_algo:
    def __init__(self, X=None, W=None, H=None, Z=None, R_s=None, R_b=None,
                 decoder=None, niter_MCEM=100, niter_MH=40, burnin=30,
                 var_MH=0.01):

        self.X = X # Mixture STFT, shape (F, N, I)
        self.XX = np.zeros(self.X.shape + (self.X.shape[-1],), dtype=complex)
        self.XX[:,:,0,0] = np.abs(self.X[:,:,0])**2
        self.XX[:,:,0,1] = self.X[:,:,0]*self.X[:,:,1].conj()
        self.XX[:,:,1,0] = self.X[:,:,1]*self.X[:,:,0].conj()
        self.XX[:,:,1,1] = np.abs(self.X[:,:,1])**2
        self.W = W # NMF dictionary matrix, shape (F, K)
        self.H = H # NMF activation matrix, shape (K, N)
        self.Vb = self.W @ self.H # Noise variance, shape (F, N)
        self.R_s = R_s # speech SCM, shape (F, I, I)
        self.R_b = R_b # noise SCM, shape (F, I, I)
        self.Z = Z # Last draw of the latent variables, shape (L, N)
        self.decoder = decoder # VAE decoder, keras model
        self.Vs = np.exp(self.decoder.decode(self.Z.T).T) # output of the
        # decoder with self.Z as input, shape (F, N)
        self.g = np.ones((1,self.X.shape[1])) # gain parameters, shape (1,N)
        self.Sigma_s = ( (self.g*self.Vs)[:,:,np.newaxis, np.newaxis] *
                        np.expand_dims(self.R_s, axis=1) ) # (F, N, I, I)
        self.Sigma_b = ( self.Vb[:,:,np.newaxis, np.newaxis] *
                        np.expand_dims(self.R_b, axis=1) ) # (F, N, I, I)
        self.Sigma_x = self.Sigma_s + self.Sigma_b # (F, N, I, I)

        self.niter_MCEM = niter_MCEM # Maximum number of MCEM iterations
        self.niter_MH = niter_MH # Number of iterations for the MH algorithm
        # of the E-step
        self.burnin = burnin # Burn-in period for the MH algorithm of the
        # E-step
        self.var_MH = var_MH # Variance of the proposal distribution of the MH
        # algorithm

    def metropolis_hastings(self, niter_MH=None, burnin=None, verbose=False):

        if niter_MH==None:
           niter_MH = self.niter_MH

        if burnin==None:
           burnin = self.burnin

        F, N, I = self.X.shape
        L = self.Z.shape[0]

        Z_sampled = np.zeros((L, N, niter_MH - burnin))

        cpt = 0
        averaged_acc_rate = 0
        for n in np.arange(niter_MH):

            Z_prime = self.Z + np.sqrt(self.var_MH)*np.random.randn(L,N)

            Vs_prime = np.exp(self.decoder.decode(Z_prime.T).T)
            # shape (F, N)

            Sigma_s_prime = ( (self.g*Vs_prime)[:,:,np.newaxis, np.newaxis] *
                        np.expand_dims(self.R_s, axis=1) ) # (F, N, I, I)

            Sigma_x_prime = Sigma_s_prime + self.Sigma_b # (F, N, I, I)

            log_acc_prob = (
                    np.sum(log_pdf_likelihood(self.X, Sigma_x_prime) -
                           log_pdf_likelihood(self.X, self.Sigma_x),
                           axis=0)
                    + log_pdf_prior(Z_prime) - log_pdf_prior(self.Z) )



            is_acc = np.log(np.random.rand(N)) < log_acc_prob
            averaged_acc_rate += np.sum(is_acc)/np.prod(is_acc.shape) \
                                 *100/niter_MH

            self.Z[:,is_acc] = Z_prime[:,is_acc]
            self.Vs = np.exp(self.decoder.decode(self.Z.T).T)
            self.Sigma_s = ( (self.g*self.Vs)[:,:,np.newaxis, np.newaxis] *
                            np.expand_dims(self.R_s, axis=1) )
            self.Sigma_x = self.Sigma_s + self.Sigma_b

            if n > burnin - 1:
                Z_sampled[:,:,cpt] = self.Z
                cpt += 1
        if verbose:
            print('averaged acceptance rate: %f' % (averaged_acc_rate))
        return Z_sampled

    def run(self, hop, wlen, win, tol=1e-4, verbose=True, separate=False):

        F, N, I = self.X.shape
        L = self.Z.shape[0]

        cost_after_M_step = np.zeros((self.niter_MCEM, 1))

        R = self.niter_MH-self.burnin

        for n in np.arange(self.niter_MCEM):

            if verbose:
                print('iteration %d/%d' % (n+1, self.niter_MCEM))

            # MC-Step
            if verbose:
                print('Metropolis-Hastings')
            Z_sampled = self.metropolis_hastings(self.niter_MH, self.burnin, verbose)
            Z_sampled = Z_sampled.reshape(L, N*R)
            Vs_multi_samples = np.exp(self.decoder.decode(Z_sampled.T).T)
            Vs_multi_samples = Vs_multi_samples.reshape((F,N,R))  # shape (F,N,R)
            Vs_multi_samples = np.transpose(Vs_multi_samples, [-1,0,1]) # shape (R,F,N)

            Sigma_s_multi_samples = ( (self.g*Vs_multi_samples)[:,:,:,np.newaxis, np.newaxis]
            * np.expand_dims(self.R_s, axis=1) ) # shape (R, F, N, I, I)

            if verbose:
                print('Update W')
            Sigma_x_multi_samples = ( self.Sigma_b + Sigma_s_multi_samples )

            inv_Sigma_x_multi_samples = multi_inv(Sigma_x_multi_samples)

            Rb_inv_Sigma_x_multi_samples = multi_mult(
                    np.expand_dims(self.R_b, axis=1), inv_Sigma_x_multi_samples)

            tr_den = np.real( np.sum(
                    multi_trace( Rb_inv_Sigma_x_multi_samples ), axis=0) )

            tr_num = np.real( np.sum(
                    multi_trace(
                            multi_mult(self.XX,
                                       multi_mult(inv_Sigma_x_multi_samples,
                                                  Rb_inv_Sigma_x_multi_samples))), axis=0) )


            self.W = self.W * np.sqrt((tr_num @ self.H.T)/(tr_den @ self.H.T))

            if np.min(self.W) < 0:
                raise NameError('Negative value in W')

            self.Vb = self.W @ self.H
            self.Sigma_b = ( self.Vb[:,:,np.newaxis, np.newaxis] *
                        np.expand_dims(self.R_b, axis=1) )

            if verbose:
                print('Update H')

            Sigma_x_multi_samples = ( self.Sigma_b + Sigma_s_multi_samples )

            inv_Sigma_x_multi_samples = multi_inv(Sigma_x_multi_samples)

            Rb_inv_Sigma_x_multi_samples = multi_mult(
                    np.expand_dims(self.R_b, axis=1),inv_Sigma_x_multi_samples)

            tr_den = np.real( np.sum(
                    multi_trace( Rb_inv_Sigma_x_multi_samples ), axis=0) )

            tr_num = np.real( np.sum(
                    multi_trace(
                            multi_mult(self.XX,
                                       multi_mult(inv_Sigma_x_multi_samples,
                                                  Rb_inv_Sigma_x_multi_samples))), axis=0) )

            self.H = self.H * np.sqrt((self.W.T @ tr_num)/(self.W.T @ tr_den ))

            if np.min(self.H) < 0:
                raise NameError('Negative value in H')

            self.Vb = self.W @ self.H
            self.Sigma_b = ( self.Vb[:,:,np.newaxis, np.newaxis] *
                        np.expand_dims(self.R_b, axis=1) )

            del Rb_inv_Sigma_x_multi_samples, tr_den, tr_num

            if verbose:
                print('Update g')

            Sigma_x_multi_samples = ( self.Sigma_b + Sigma_s_multi_samples )

            inv_Sigma_x_multi_samples = multi_inv(Sigma_x_multi_samples)

            Rs_inv_Sigma_x_multi_samples = multi_mult(
                    np.expand_dims(self.R_s, axis=1),inv_Sigma_x_multi_samples)

            den = np.real( np.sum(Vs_multi_samples *
                         multi_trace( Rs_inv_Sigma_x_multi_samples ), axis=(0,1)) )

            num = np.real( np.sum( Vs_multi_samples *
                    multi_trace(
                            multi_mult(self.XX,
                                       multi_mult(inv_Sigma_x_multi_samples,
                                                  Rs_inv_Sigma_x_multi_samples))), axis=(0,1)) )

            self.g = self.g * np.sqrt(num/den)

            if np.min(self.g) < 0:
                raise NameError('Negative value in g')

            Sigma_s_multi_samples = ( (self.g*Vs_multi_samples)[:,:,:,np.newaxis, np.newaxis]
            * np.expand_dims(self.R_s, axis=1) )

            del num, den, inv_Sigma_x_multi_samples

            if verbose:
                print('Update R_s')

            Sigma_x_multi_samples = ( self.Sigma_b + Sigma_s_multi_samples )
            inv_Sigma_x_multi_samples = multi_inv(Sigma_x_multi_samples)
            A_rn = ( (self.g*Vs_multi_samples)[:,:,:,np.newaxis, np.newaxis] *
                    inv_Sigma_x_multi_samples )
            A = np.sum(A_rn, axis=(0,2))
            B = multi_mult(
                    multi_mult(self.R_s,
                               np.sum(multi_mult(A_rn,
                                                 multi_mult(self.XX,
                                                            inv_Sigma_x_multi_samples)),
                                                            axis=(0,2))),
                    self.R_s)


            for f in np.arange(F):
                self.R_s[f,:,:] = ARE_solver(A[f,:,:], B[f,:,:], force_sym=True)
                self.R_s[f,:,:] = self.R_s[f,:,:] + 1e-12*np.eye(I)

            Sigma_s_multi_samples = ( (self.g*Vs_multi_samples)[:,:,:,np.newaxis, np.newaxis]
            * np.expand_dims(self.R_s, axis=1) )

            if verbose:
                print('Update R_b')

            Sigma_x_multi_samples = ( self.Sigma_b + Sigma_s_multi_samples )
            inv_Sigma_x_multi_samples = multi_inv(Sigma_x_multi_samples)
            A_rn = ( self.Vb[:,:,np.newaxis, np.newaxis] *
                    inv_Sigma_x_multi_samples )
            A = np.sum(A_rn, axis=(0,2))
            B = multi_mult(
                    multi_mult(self.R_b,
                               np.sum(multi_mult(A_rn,
                                                 multi_mult(self.XX,
                                                            inv_Sigma_x_multi_samples)),
                                                            axis=(0,2))),
                    self.R_b)

            for f in np.arange(F):
                self.R_b[f,:,:] = ARE_solver(A[f,:,:], B[f,:,:], force_sym=True)
                self.R_b[f,:,:] = self.R_b[f,:,:] + 1e-12*np.eye(I)


            self.Sigma_b = ( self.Vb[:,:,np.newaxis, np.newaxis] *
                    np.expand_dims(self.R_b, axis=1) )

            if verbose:
                print('Normalize')
            self.normalize()

            # Compute cost
            Sigma_x_multi_samples = ( self.Sigma_b + Sigma_s_multi_samples )
            inv_Sigma_x_multi_samples = multi_inv(Sigma_x_multi_samples)

            det_Sigma_x_multi_samples = np.real(multi_det(Sigma_x_multi_samples))

            cost_after_M_step[n] = np.mean(
                    np.real(multi_trace(
                            multi_mult(self.XX,
                                       inv_Sigma_x_multi_samples)))
                    + np.log(det_Sigma_x_multi_samples) )

            # separate
            if separate:
                Sigma_s_multi_samples = ( (self.g*Vs_multi_samples)[:,:,:,np.newaxis, np.newaxis]
                * np.expand_dims(self.R_s, axis=1) )

                Sigma_x_multi_samples = ( self.Sigma_b + Sigma_s_multi_samples )
                inv_Sigma_x_multi_samples = multi_inv(Sigma_x_multi_samples)

                self.Y_s_hat = multi_mult( np.mean(
                        multi_mult(Sigma_s_multi_samples,
                                   inv_Sigma_x_multi_samples), axis=0), self.X) # shape (F,N,I)

                self.Y_b_hat = multi_mult( np.mean(
                        multi_mult(self.Sigma_b,
                                   inv_Sigma_x_multi_samples), axis=0), self.X) # shape (F,N,I)



            if verbose:
                print('cost=%.4f\n' % cost_after_M_step[n])

            if n>0 and cost_after_M_step[n-1] - cost_after_M_step[n] < tol:
                if verbose:
                    print('tolerance achieved')
                break

        return cost_after_M_step, n

    def normalize(self):

        tr_R_b = multi_trace(self.R_b) # (F,1)
        self.R_b = self.R_b/tr_R_b[:,np.newaxis, np.newaxis]

        self.W = self.W*tr_R_b[:,np.newaxis]

        norm_col_W = np.sum(np.abs(self.W), axis=0)
        self.W = self.W/norm_col_W[np.newaxis,:]
        self.H = self.H*norm_col_W[:,np.newaxis]
        self.Vb = self.W @ self.H

    def separate(self, niter_MH=None, burnin=None, verbose=False):

        if niter_MH==None:
           niter_MH = self.niter_MH

        if burnin==None:
           burnin = self.burnin

        F, N, I = self.X.shape
        L = self.Z.shape[0]

        Z_sampled = self.metropolis_hastings(niter_MH, burnin, verbose)
        R = niter_MH - burnin
        Z_sampled = Z_sampled.reshape(L, N*R)
        Vs_multi_samples = np.exp(self.decoder.decode(Z_sampled.T).T)
        Vs_multi_samples = Vs_multi_samples.reshape((F,N,R))  # shape (F,N,R)
        Vs_multi_samples = np.transpose(Vs_multi_samples, [-1,0,1]) # shape (R,F,N)

        Sigma_s_multi_samples = ( (self.g*Vs_multi_samples)[:,:,:,np.newaxis, np.newaxis]
        * np.expand_dims(self.R_s, axis=1) )

        Sigma_x_multi_samples = ( self.Sigma_b + Sigma_s_multi_samples )
        inv_Sigma_x_multi_samples = multi_inv(Sigma_x_multi_samples)

        self.Y_s_hat = multi_mult( np.mean(
                multi_mult(Sigma_s_multi_samples,
                           inv_Sigma_x_multi_samples), axis=0), self.X) # shape (F,N,I)

        self.Y_b_hat = multi_mult( np.mean(
                multi_mult(self.Sigma_b,
                           inv_Sigma_x_multi_samples), axis=0), self.X) # shape (F,N,I)


def log_pdf_likelihood(x, Sigma):
    """
    x: shape (F,N,2)
    Sigma: shape (F,N,2,2)

    return log_pdf (F,N)
    """
    eps = np.finfo(float).eps # machine epsilon

    det_pi_Sigma = multi_det(np.pi*Sigma)
    invSigma = multi_inv(Sigma)
    log_pdf = (- np.log(np.real(det_pi_Sigma) + eps) -
               np.real(multi_mult(x.conj(), multi_mult(invSigma, x))) )


    return log_pdf


def log_pdf_prior(z):
    """
    z: shape (L,N)

    return log_pdf (N,)
    """
    log_pdf = -.5*( np.log((2*np.pi)**2) + np.sum(z**2, axis=0) )

    return log_pdf



#x = np.zeros((F, N, I), dtype=complex)
#mu = np.zeros((F, N, I), dtype=complex)
#Sigma = np.tile(np.eye(I), (F, N, 1, 1))
#res = log_pdf_multiv_complex_prop_Gaussian(x, mu, Sigma)
