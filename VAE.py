#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Copyright (c) 2019 by Inria
Authored by Simon Leglaive (simon.leglaive@inria.fr)
License agreement in LICENSE.txt
"""

import numpy as np
from keras.layers import Input, Dense, Lambda, BatchNormalization, ReLU
from keras.models import Model
from keras import backend as KB
import keras as K
import matplotlib.pyplot as plt

class VAE:

    def __init__(self, input_dim=None, latent_dim=None, intermediate_dim=None, batch_size=None):
        self.input_dim = input_dim
        self.latent_dim = latent_dim
        self.intermediate_dim = intermediate_dim
        self.batch_size = batch_size
        self.model = None
        self.history = None


    def build(self):
        """ Build the network according to the attributes of the class.
        """

        #### Define encoder ####

        # Define input layer (Type: Tensor)
        input_encoder = Input(batch_shape=(self.batch_size, self.input_dim), name='input_layer_encoder')

        # List of the outputs of the intermediate layers (Type: Tensor)
        output_intermediate_layers_encoder = [None] * len(self.intermediate_dim)
        output_intermediate_layers_encoder_relu = [None] * len(self.intermediate_dim)
        output_intermediate_layers_encoder_relu_BN = [None] * len(self.intermediate_dim)

        for n, current_intermediate_dim in enumerate(self.intermediate_dim):
            intermediate_layer_name_encoder = 'intermediate_layer_' + str(n) + '_encoder'
            if n==0:
                # Link to input layer
                output_intermediate_layers_encoder[n] = Dense(current_intermediate_dim,
                                                              use_bias=True,
                                                              kernel_initializer=K.initializers.VarianceScaling(scale=1.0,
                                                                                                                mode='fan_in',
                                                                                                                distribution='normal',
                                                                                                                seed=None),
                                                              name=intermediate_layer_name_encoder)(input_encoder)
            else:
                # Linked to previous intermediate layer
                output_intermediate_layers_encoder[n] = Dense(current_intermediate_dim,
                                                              use_bias=True,
                                                              kernel_initializer=K.initializers.VarianceScaling(scale=1.0,
                                                                                                                mode='fan_in',
                                                                                                                distribution='normal',
                                                                                                                seed=None),
                                                              name=intermediate_layer_name_encoder)(output_intermediate_layers_encoder_relu_BN[n-1])

            output_intermediate_layers_encoder_relu[n] = ReLU(max_value=None)(output_intermediate_layers_encoder[n])

            output_intermediate_layers_encoder_relu_BN[n] = BatchNormalization(axis=-1,
                                                                               momentum=0.99,
                                                                               epsilon=0.001,
                                                                               center=True,
                                                                               scale=True,
                                                                               beta_initializer='zeros',
                                                                               gamma_initializer='ones',
                                                                               moving_mean_initializer='zeros',
                                                                               moving_variance_initializer='ones',
                                                                               beta_regularizer=None,
                                                                               gamma_regularizer=None,
                                                                               beta_constraint=None,
                                                                               gamma_constraint=None)(output_intermediate_layers_encoder_relu[n])

        # Output layer of the encoder: latent variable mean (Type: Tensor)
        latent_mean = Dense(self.latent_dim,
                            kernel_initializer=K.initializers.VarianceScaling(scale=1.0, mode='fan_in', distribution='normal', seed=None),
                            name='latent_mean')(output_intermediate_layers_encoder_relu_BN[-1])

        # Output layer of the encoder: logarithm of the latent variable variance (Type: Tensor)
        latent_log_var = Dense(self.latent_dim,
                               kernel_initializer=K.initializers.VarianceScaling(scale=1.0, mode='fan_in', distribution='normal', seed=None),
                               name='latent_log_var')(output_intermediate_layers_encoder_relu_BN[-1])

        #### Define sampling layer ####

        # Sampling function
        def sampling(args):
            z_mean, z_log_var = args
            epsilon = KB.random_normal(shape=(self.batch_size, self.latent_dim), mean=0.,stddev=1.0)
            return z_mean + KB.exp(z_log_var / 2) * epsilon

        # Wrap the sampling function as a layer in Keras (Type: Tensor)
        z = Lambda(sampling)([latent_mean, latent_log_var])

        #### Define decoder ####

        # List of the outputs of the intermediate layers (Type: Tensor)
        output_intermediate_layers_decoder = [None] * len(self.intermediate_dim)
        output_intermediate_layers_decoder_relu = [None] * len(self.intermediate_dim)
        output_intermediate_layers_decoder_relu_BN = [None] * len(self.intermediate_dim)

        for n, current_intermediate_dim in enumerate(list(reversed(self.intermediate_dim))):
            intermediate_layer_name_decoder = 'intermediate_layer_' + str(n) + '_decoder'
            if n==0:
                # Link to sampled latent variable layer
                output_intermediate_layers_decoder[n] = Dense(current_intermediate_dim,
                                                              use_bias=True,
                                                              kernel_initializer=K.initializers.VarianceScaling(scale=1.0,
                                                                                                                mode='fan_in',
                                                                                                                distribution='normal',
                                                                                                                seed=None),
                                                              name=intermediate_layer_name_decoder)(z)
            else:
                # Link to previous intermediate layer
                output_intermediate_layers_decoder[n] = Dense(current_intermediate_dim,
                                                              use_bias=True,
                                                              kernel_initializer=K.initializers.VarianceScaling(scale=1.0,
                                                                                                                mode='fan_in',
                                                                                                                distribution='normal',
                                                                                                                seed=None),
                                                              name=intermediate_layer_name_decoder)(output_intermediate_layers_decoder_relu_BN[n-1])

            output_intermediate_layers_decoder_relu[n] = ReLU(max_value=None)(output_intermediate_layers_decoder[n])

            output_intermediate_layers_decoder_relu_BN[n] = BatchNormalization(axis=-1,
                                                                               momentum=0.99,
                                                                               epsilon=0.001,
                                                                               center=True,
                                                                               scale=True,
                                                                               beta_initializer='zeros',
                                                                               gamma_initializer='ones',
                                                                               moving_mean_initializer='zeros',
                                                                               moving_variance_initializer='ones',
                                                                               beta_regularizer=None,
                                                                               gamma_regularizer=None,
                                                                               beta_constraint=None,
                                                                               gamma_constraint=None)(output_intermediate_layers_decoder_relu[n])

        # Output layer of the decoder: log variance of x|z (Type: Tensor)
        output_decoder = Dense(self.input_dim,
                               kernel_initializer=K.initializers.VarianceScaling(scale=1.0,
                                                                                 mode='fan_in',
                                                                                 distribution='normal',
                                                                                 seed=None),
                               name='output_layer_decoder')(output_intermediate_layers_decoder_relu_BN[-1])

        self.model = Model(input_encoder, output_decoder)

    def compile(self, optimizer='adam', loss=None, metrics=None):
        """ Compile the model

        Parameters
        ----------
        optimizer   :   string
                        The name of the optimizer as proposed in Keras
        loss        :   string or function
                        The name of objective function as proposed in Keras or an objective function

        Returns
        -------
        Nothing

        """
        if metrics==None:
            self.model.compile(optimizer=optimizer, loss=loss)
        else:
            self.model.compile(optimizer=optimizer, loss=loss, metrics=metrics)

    def train(self, x_train=None, y_train=None, x_val=None, y_val=None,
              verbose=1, shuffle=True, epochs=500, early_stopping_dic=None):

        """ Train the model

        Parameters
        ----------
        x_train             :  array, shape (number of examples, dimension)
        y_train             :  array, shape (number of examples, dimension)
        x_val            :  array, shape (number of examples, dimension)
        y_val             :  array, shape (number of examples, dimension)
        verbose             :  integer
        shuffle             :  boolean
        epochs              :  integer
        early_stopping_dic  :  dictionary


        Returns
        -------
        history             :  History object

        """

        if early_stopping_dic != None:
            # Define callback earlystopping
            early_stopping = K.callbacks.EarlyStopping(
                    monitor=early_stopping_dic['monitor'],
                    min_delta=early_stopping_dic['min_delta'],
                    patience=early_stopping_dic['patience'],
                    verbose=early_stopping_dic['verbose'],
                    mode=early_stopping_dic['mode'])

            checkpoint = K.callbacks.ModelCheckpoint(
                    early_stopping_dic['weights_file'],
                    monitor=early_stopping_dic['monitor'],
                    verbose=early_stopping_dic['verbose'],
                    save_best_only=True,
                    mode=early_stopping_dic['mode'],
                    save_weights_only=True)

            # Train the VAE
            self.history = self.model.fit(x_train, y_train,
                              verbose=verbose,
                              callbacks=[checkpoint, early_stopping],
                              shuffle=shuffle,
                              epochs=epochs,
                              batch_size=self.batch_size,
                              validation_data=(x_val, y_val))
        else:
            # Train the VAE
            self.history = self.model.fit(x_train, y_train,
                              verbose=verbose,
                              shuffle=shuffle,
                              epochs=epochs,
                              batch_size=self.batch_size,
                              validation_data=(x_val, y_val))

    def print_model(self):
        """ Print the network """
        self.model.summary()

    def save_weights(self, path_to_file):
        """ Save the network weights to a .h5 file specified by 'path_to_file' """
        self.model.save_weights(path_to_file)

    def load_weights(self, path_to_file):
        self.model.load_weights(path_to_file)


    def plot_losses(self):
        """ plot the training and validation losses along the epochs """
        train_loss = self.history.history['loss']
        val_loss = self.history.history['val_loss']
        plt.figure()
        plt.subplot(2, 1, 1)
        plt.plot(train_loss)
        plt.ylabel('training loss')
        plt.xlabel('epoch')
        plt.subplot(2, 1, 2)
        plt.plot(val_loss)
        plt.ylabel('validation loss')
        plt.xlabel('epoch')

        plt.show()

    def plot_IS_divergence(self):
        """ plot the training and validation IS divergences along the epochs """
        train_loss = self.history.history['IS_divergence']
        val_loss = self.history.history['val_IS_divergence']
        plt.figure()
        plt.subplot(2, 1, 1)
        plt.plot(train_loss)
        plt.ylabel('training IS divergence')
        plt.xlabel('epoch')
        plt.subplot(2, 1, 2)
        plt.plot(val_loss)
        plt.ylabel('validation IS divergence')
        plt.xlabel('epoch')

        plt.show()

    def encode_decode(self, data):
        """ Encode the data in the latent space and reconstruct them from the mean of the latent variable """

        # Encode the data in the latent space
        encoder = Encoder(batch_size=self.batch_size)
        encoder.build(self)
        latent_mean, latent_log_var = encoder.encode(data)

        # Decode the data from the mean of the latent variable
        decoder = Decoder()
        decoder.build(self)
        return np.exp(decoder.decode(latent_mean))


    def encode_decode_with_sampling(self, data):
        """ Encode the data in the latent space and reconstruct them from the mean of the latent variable """

        # Encode the data in the latent space
        encoder = Encoder(batch_size=self.batch_size)
        encoder.build(self)
        latent_mean, latent_log_var = encoder.encode(data)

        latent_sampled = np.zeros((latent_mean.shape[0], latent_mean.shape[1]))
        n_sample = 10
        for n in np.arange(n_sample):
            epsilon = np.random.randn(latent_mean.shape[0], latent_mean.shape[1])
            latent_sampled = latent_sampled + latent_mean + np.exp(latent_log_var/.5) * epsilon
        latent_sampled = latent_sampled/n_sample

        # Decode the data from the mean of the latent variable
        decoder = Decoder()
        decoder.build(self)
        return np.exp(decoder.decode(latent_sampled))


class Encoder:
    def __init__(self, batch_size):
        self.model_mean = None
        self.model_log_var = None
        self.batch_size = batch_size

    def build(self, vae):
        self.model_mean = Model(vae.model.get_layer('input_layer_encoder').output, vae.model.get_layer('latent_mean').output)
        self.model_log_var = Model(vae.model.get_layer('input_layer_encoder').output, vae.model.get_layer('latent_log_var').output)

    def encode(self, data):
        latent_mean = self.model_mean.predict(data, batch_size=self.batch_size)
        latent_log_var = self.model_log_var.predict(data, batch_size=self.batch_size)
        return latent_mean, latent_log_var

class Decoder:
    def __init__(self, batch_size):
        self.model_var = None
        self.batch_size = batch_size

    def build(self, vae):

        ind_first_layer_decoder = None
        layer_list = vae.model.layers
        for n, layer in enumerate(layer_list):
            if layer.name == 'intermediate_layer_0_decoder':
                ind_first_layer_decoder = n
                break

        if ind_first_layer_decoder==None:
            raise NameError('intermediate_layer_0_decoder not found')

        # Input layer with dimension the size of the latent space
        decoder_input = Input(shape=(vae.latent_dim,))

        # List of the outputs of the intermediate layers (Type: Tensor)
        output_intermediate_layers_decoder = [None] * len(vae.intermediate_dim)
        output_intermediate_layers_decoder_relu = [None] * len(vae.intermediate_dim)
        output_intermediate_layers_decoder_relu_BN = [None] * len(vae.intermediate_dim)

        cpt = 0
        for n in np.arange(ind_first_layer_decoder, len(vae.model.layers)-1, 3):
            if n==ind_first_layer_decoder:
                # Link to sampled latent variable layer
                output_intermediate_layers_decoder[cpt] = vae.model.get_layer(index=n)(decoder_input)
            else:
                # Link to previous intermediate layer
                output_intermediate_layers_decoder[cpt] = vae.model.get_layer(index=n)(output_intermediate_layers_decoder_relu_BN[cpt-1])

            output_intermediate_layers_decoder_relu[cpt] = vae.model.get_layer(index=n+1)(output_intermediate_layers_decoder[cpt])

            output_intermediate_layers_decoder_relu_BN[cpt] = vae.model.get_layer(index=n+2)(output_intermediate_layers_decoder_relu[cpt])

            cpt += 1

        decoder_output = vae.model.get_layer(index=len(vae.model.layers)-1)(output_intermediate_layers_decoder_relu_BN[-1])

        # Define the decoder model
        self.model_var = Model(decoder_input, decoder_output)

    def decode(self, data):
        return self.model_var.predict(data, batch_size=self.batch_size)
