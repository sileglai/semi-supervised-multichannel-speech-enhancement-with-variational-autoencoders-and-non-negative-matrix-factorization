#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Copyright (c) 2019 by Inria
Authored by Simon Leglaive (simon.leglaive@inria.fr)
License agreement in LICENSE.txt
"""

import os
my_seed = 0
import numpy as np
np.random.seed(my_seed)
import librosa
from keras import backend as KB
import keras as K
import matplotlib.pyplot as plt
from data_tools import (compute_STFT_data_from_file_list_TIMIT,
                        load_STFT_data_in_array_TIMIT,
                        write_VAE_params_to_text_file,
                        write_VAE_params_to_pckl_file)
import pickle
import datetime
from VAE import VAE

#%% Define paths

# Directory containing the training set of the TIMIT dataset
TIMIT_train_folder = '/local_scratch/sileglai/datasets/clean_speech/TIMIT/TRAIN'

# Pickle file containing the training data
data_dir = 'data'
data_file = os.path.join(data_dir, 'training_data.pckl')

# Directory for writting the results of the training
results_dir = 'training_results'

# Create the directories if necessary
if not(os.path.isdir(data_dir)):
    os.makedirs(data_dir)

if not(os.path.isdir(results_dir)):
    os.makedirs(results_dir)

weights_file = os.path.join(results_dir, 'saved_weights.h5')

#%% Compute training data

fs = int(16e3) # Sampling rate
wlen_sec = 64e-3 # STFT window length in seconds
hop_percent = 0.25  # hop size as a percentage of the window length
trim = True # trim leading and trailing silences

# List containing all the wav files in the training set of TIMIT
# The number of elements should be 4620 (462 speakers x 10 sentences)
file_list = librosa.util.find_files(TIMIT_train_folder, ext='wav')

compute_STFT_data_from_file_list_TIMIT(file_list,
                                        fs=fs,
                                        wlen_sec=wlen_sec,
                                        hop_percent=hop_percent,
                                        zp_percent=0,
                                        trim=trim,
                                        verbose=True,
                                        out_file=data_file)

#%% Parameters

description = 'Using the entire training database of TIMIT'

input_representation = 'log_power_spec'
input_normalization = 'zero_mean_unit_std'
output_representation = 'log_power_spec'
latent_dim = 32
intermediate_dim = [512, 128] # list of the hidden-layer sizes for the encoder (the decoder is symmetric)
batch_size = 128
epochs = 500
shuffle = True
early_stopping_patience = 10
monitor_early_stopping = 'val_loss'
valid_set_percent = 0.2

date = datetime.datetime.now().strftime("%Y-%m-%d-%Hh%M")

verbose_data = True
verbose_vae = 2 # Verbosity mode. 0 = silent, 1 = progress bar, 2 = one line per epoch.
display_network = True

eps = 1e-16

plot_reconstruction = True

#%% Load the training and validation data

num_files_tot = None # If None, all the files are considered

[power_spec, phase, data_info, fs, wlen_sec, hop_percent, trim, num_files_tot] = load_STFT_data_in_array_TIMIT(data_file,
                                                                                            ind_first_file=0,
                                                                                            num_files=num_files_tot,
                                                                                            verbose=verbose_data)
N_tot = power_spec.shape[-1]

num_files_train = int((1-valid_set_percent)*num_files_tot) # We take 80 % of the files for training

ind_beg_val = data_info[num_files_train]['index_begin'] # Get the index of the first frame of the first file in the validation set

N_train = ind_beg_val//batch_size*batch_size # Make the number of training examples a multiple of the batch_size

N_val = (N_tot - ind_beg_val)//batch_size*batch_size # Make the number of validation examples a multiple of the batch_size

x_train = power_spec[:,0:N_train].T
phase_train = phase[:,0:N_train]

x_val = power_spec[:,ind_beg_val:ind_beg_val+N_val].T
phase_val = phase[:,N_train:]

del power_spec, phase

y_train = x_train # target training
y_val = x_val # target validation

# Input data for training: log-spectrogram w/ zero mean and unit std
x_train = np.log(x_train + eps)
norm_mean = np.mean(x_train, axis=0)
x_train = x_train - norm_mean
norm_std = np.std(x_train, axis=0)
x_train = x_train/(norm_std + eps)

# Input data for validation: log-spectrogram w/ zero mean and unit std
x_val = (np.log(x_val) - norm_mean)/norm_std

if np.any(np.isnan(x_train)):
    raise NameError('nan in x_train')

if np.any(np.isinf(x_train)):
    raise NameError('inf in x_train')

if np.any(np.isnan(x_val)):
    raise NameError('nan in x_val')

if np.any(np.isinf(x_val)):
    raise NameError('inf in x_val')

#%%  Build the VAE

input_dim = x_train.shape[-1]

vae = VAE(input_dim=input_dim, latent_dim=latent_dim, intermediate_dim=intermediate_dim, batch_size=batch_size)

vae.build()

if display_network:
    vae.print_model()

#%% Train the VAE

# Define loss
def vae_loss(data_orig, data_reconstructed):
    delta = 1e-10
    # Reconstruction term, equivalent to the IS divergence between x_orig and x_reconstructed
    reconstruction_loss = KB.mean( (data_orig + delta)/(KB.exp(data_reconstructed) + delta) + data_reconstructed, axis=-1)
    # Regularization term
    kl_loss = - 0.5 * KB.mean(vae.model.get_layer('latent_log_var').output
                                - KB.square(vae.model.get_layer('latent_mean').output)
                                - KB.exp(vae.model.get_layer('latent_log_var').output), axis=-1)

    return reconstruction_loss + kl_loss


# Compile model
adam = K.optimizers.Adam(lr=1e-3, beta_1=0.9, beta_2=0.999, epsilon=None, decay=0, amsgrad=False)
vae.compile(optimizer=adam, loss=vae_loss)

# Train VAE
if early_stopping_patience==None:
    history = vae.train(x_train=x_train,
                        y_train=y_train,
                        x_val=x_val,
                        y_val=y_val,
                        verbose=verbose_vae,
                        shuffle=shuffle,
                        epochs=epochs)
else:
    early_stopping_dic = {'monitor':monitor_early_stopping, 'min_delta':0,
                          'patience':early_stopping_patience, 'verbose':1,
                          'mode':'min', 'weights_file':weights_file}
    history = vae.train(x_train=x_train,
                        y_train=y_train,
                        x_val=x_val,
                        y_val=y_val,
                        verbose=verbose_vae,
                        shuffle=shuffle,
                        epochs=epochs,
                        early_stopping_dic=early_stopping_dic)


#%% Save parameters

dic_params = {'input_representation':input_representation,
              'input_normalization':input_normalization,
              'output_representation':output_representation,
              'input_dim':input_dim,
              'latent_dim':latent_dim,
              'intermediate_dim':intermediate_dim,
              'batch_size':batch_size,
              'epochs':epochs,
              'shuffle':shuffle,
              'early_stopping_patience':early_stopping_patience,
              'monitor_early_stopping':monitor_early_stopping,
              'my_seed':my_seed,
              'description':description,
              'valid_set_percent':valid_set_percent,
              'date':date,
              'norm_mean':norm_mean,
              'norm_std':norm_std}

# Write the VAE and training parameters to a text file
parms_text_file = os.path.join(results_dir, 'parameters.txt')
write_VAE_params_to_text_file(parms_text_file, dic_params)

# Write the VAE and training parameters to a pickle file
parms_pckl_file = os.path.join(results_dir, 'parameters.pckl')
write_VAE_params_to_pckl_file(parms_pckl_file, dic_params)

#%% Save the training loss

loss_file = os.path.join(results_dir, 'training_loss.pckl')

with open(loss_file, 'wb') as f:
        pickle.dump(vae.history.history, f)

#%% Check the reconstruction on a few frames of the validation set

if plot_reconstruction:

    wlen = wlen_sec*fs # window length of 64 ms
    wlen = np.int(np.power(2, np.ceil(np.log2(wlen)))) # next power of 2
    hop = np.int(wlen*hop_percent) # hop size

    data_decoded = vae.encode_decode(x_val[0:512,:])

    plt.figure()
    plt.subplot(2, 1, 1)
    x_val_unstandardized = np.exp(x_val[0:512,:]*norm_std + norm_mean)
    librosa.display.specshow(librosa.power_to_db(x_val_unstandardized[0:512,:].T), y_axis='log', sr=fs, hop_length=hop, vmin=-50, vmax=20)
    plt.set_cmap('jet')
    plt.colorbar(format='%+2.0f dB')
    plt.title('Original spectrogram')

    plt.subplot(2, 1, 2)
    librosa.display.specshow(librosa.power_to_db(data_decoded.T), y_axis='log', sr=fs, hop_length=hop, vmin=-50, vmax=20)
    plt.set_cmap('jet')
    plt.colorbar(format='%+2.0f dB')
    plt.title('Reconstructed spectrogram')
