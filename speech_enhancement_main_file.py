#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Copyright (c) 2019 by Inria
Authored by Simon Leglaive (simon.leglaive@inria.fr)
License agreement in LICENSE.txt
"""

import numpy as np
import librosa
import os
import pickle
from MCEM_algo import MCEM_algo
from VAE import VAE, Decoder, Encoder
import soundfile as sf

#%% Parameters

mix_path = './audio/mix.wav'
results_dir = './audio'
#niter_MCEM = 500
niter_MCEM = 50
niter_MH = 40
burnin = 30
var_MH = 0.001
K = 10
#tol = 0.0001
tol = 0

fs = int(16e3) # Sampling rate
eps = np.finfo(float).eps # machine epsilon
I = 2

# STFT parameters
wlen_sec = 64e-3 # STFT window length in seconds
hop_percent = 0.25  # hop size as a percentage of the window length
zp_percent=0
wlen = wlen_sec*fs # window length of 64 ms
wlen = np.int(np.power(2, np.ceil(np.log2(wlen)))) # next power of 2
hop = np.int(hop_percent*wlen) # hop size
nfft = wlen + zp_percent*wlen # number of points of the discrete Fourier transform
win = np.sin(np.arange(.5,wlen-.5+1)/wlen*np.pi); # sine analysis window

#%% Load mixture

x, fs_x = sf.read(mix_path)
try:
    x = x/np.max(np.abs(x))
except ValueError:
    pass
if fs != fs_x:
    raise ValueError('Unexpected sampling rate for the mixture signal')

T_orig = x.shape[0]

X_l = librosa.stft(x[:,0], n_fft=nfft, hop_length=hop, win_length=wlen,
                   window=win)
X_r = librosa.stft(x[:,1], n_fft=nfft, hop_length=hop, win_length=wlen,
                   window=win)
F, N = X_l.shape
X = np.zeros((F,N,I), dtype=complex)
X[:,:,0] = X_l
X[:,:,1] = X_r
del X_l, X_r

#%% Build VAE

vae_dir = './training_results/2018-08-01-10h05_with_bias_512_128_32'
parms_pckl_file = os.path.join(vae_dir, 'parameters.pckl')
dic_params = pickle.load( open( parms_pckl_file, "rb" ))
input_dim = dic_params['input_dim']
latent_dim = dic_params['latent_dim']
intermediate_dim = dic_params['intermediate_dim']
norm_mean = dic_params['norm_mean']
norm_std = dic_params['norm_std']

vae = VAE(input_dim=input_dim, latent_dim=latent_dim,
          intermediate_dim=intermediate_dim, batch_size=1)
vae.build()

# Load saved weights
weights_file = os.path.join(vae_dir, 'saved_weights.h5')
vae.load_weights(weights_file)

# Build decoder
decoder = Decoder(batch_size=N)
decoder.build(vae)

# Build encoder
encoder = Encoder(batch_size=N)
encoder.build(vae)

del vae

#%% main block

# Initialize NMF parameters
np.random.seed(0)
W_init = np.maximum(np.random.rand(F, K), eps)
H_init = np.maximum(np.random.rand(K, N), eps)

# Initialize SCM
R_s = np.tile(np.eye(I, dtype=complex), (F, 1, 1))
R_b = np.tile(np.eye(I, dtype=complex), (F, 1, 1))

# Compute the first latent variable sample
encoder_input = (np.log(np.mean(np.abs(X)**2, axis=-1) + eps) - norm_mean[:,np.newaxis])/norm_std[:,np.newaxis]
latent_mean, latent_log_var = encoder.encode(encoder_input.T)
Z_init = latent_mean.T

# Instanciate the MCEM algo
mcem_algo = MCEM_algo(X=X, W=W_init, H=H_init, Z=Z_init, R_s=R_s, R_b=R_b,
                      decoder=decoder, niter_MCEM=niter_MCEM,
                      niter_MH=niter_MH, burnin=burnin, var_MH=var_MH)

# Run MCEM algo
cost, n_final = mcem_algo.run(hop=hop, wlen=wlen, win=win, tol=tol,
                              verbose=True)
cost = cost[0:n_final]

# Estimate sources
mcem_algo.separate(niter_MH=niter_MH, burnin=burnin, verbose=False)

# Reconstruc time-domain signals
y_s_hat = np.zeros((T_orig, 2))
y_b_hat = np.zeros((T_orig, 2))

y_s_hat[:,0] = librosa.istft(stft_matrix=mcem_algo.Y_s_hat[:,:,0], hop_length=hop,
                      win_length=wlen, window=win, length=T_orig)
y_s_hat[:,1] = librosa.istft(stft_matrix=mcem_algo.Y_s_hat[:,:,1], hop_length=hop,
                      win_length=wlen, window=win, length=T_orig)

y_b_hat[:,0] = librosa.istft(stft_matrix=mcem_algo.Y_b_hat[:,:,0], hop_length=hop,
                      win_length=wlen, window=win, length=T_orig)
y_b_hat[:,1] = librosa.istft(stft_matrix=mcem_algo.Y_b_hat[:,:,1], hop_length=hop,
                      win_length=wlen, window=win, length=T_orig)

# Save estimated wav files
librosa.output.write_wav(os.path.join(results_dir, 'speech_est.wav'), y_s_hat, fs)
librosa.output.write_wav(os.path.join(results_dir,'noise_est.wav'), y_b_hat, fs)
