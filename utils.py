#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Copyright (c) 2019 by Inria
Authored by Simon Leglaive (simon.leglaive@inria.fr)
License agreement in LICENSE.txt
"""

import numpy as np
import time

def ARE_solver(A, B, force_sym=True):
    """
    Algebraic Riccati Equation solver
    Solve XAX = B for X
    """
    I = A.shape[0]

    C = np.zeros((2*I, 2*I), dtype=complex)
    C[:I, I:] = -A
    C[I:, :I] = -B

    eig_val, eig_vec = np.linalg.eig(C)

    asc_order = np.argsort(np.real(eig_val))
    eig_vec_sort = eig_vec[:,asc_order][:,:I]
#    eig_val_sort = eig_val[asc_order][:I]

    F = eig_vec_sort[:I,:]
    G = eig_vec_sort[I:,:]

    X = G@np.linalg.inv(F)

    if force_sym:
        X = (X + X.conj().T)/2

    return X

def multi_mult(A, B):
    """
    A: shape (...,2,2) or (...,2)
    B: shape (...,2,2) or (...,2)
    """
    if A.shape[-2:] == (2,2):
        A_first_slices = (np.s_[:],) * (A.ndim-2)
        if B.shape[-2:] == (2,2):
            # A_fn (2x2) x B_fn (2x2)
            B_first_slices = (np.s_[:],) * (B.ndim-2)
            if A.ndim-2 > B.ndim-2:
                AB = np.zeros(A.shape[:-2]+(2,2), dtype=complex)
            else:
                AB = np.zeros(B.shape[:-2]+(2,2), dtype=complex)
            AB_first_slices = (np.s_[:],) * (AB.ndim-2)
            AB[AB_first_slices+(0,0)] = A[A_first_slices+(0,0)]*B[B_first_slices+(0,0)] + A[A_first_slices+(0,1)]*B[B_first_slices+(1,0)]
            AB[AB_first_slices+(0,1)] = A[A_first_slices+(0,0)]*B[B_first_slices+(0,1)] + A[A_first_slices+(0,1)]*B[B_first_slices+(1,1)]
            AB[AB_first_slices+(1,0)] = A[A_first_slices+(1,0)]*B[B_first_slices+(0,0)] + A[A_first_slices+(1,1)]*B[B_first_slices+(1,0)]
            AB[AB_first_slices+(1,1)] = A[A_first_slices+(1,0)]*B[B_first_slices+(0,1)] + A[A_first_slices+(1,1)]*B[B_first_slices+(1,1)]
        else:
            # A_fn (2x2) x b_fn (2x1)
            B_first_slices = (np.s_[:],) * (B.ndim-1)
            if A.ndim-2 > B.ndim-1:
                AB = np.zeros(A.shape[:-2]+(2,), dtype=complex)
            else:
                AB = np.zeros(B.shape[:-1]+(2,), dtype=complex)
            AB_first_slices = (np.s_[:],) * (AB.ndim-1)
            AB[AB_first_slices+(0,)] = A[A_first_slices+(0,0)]*B[B_first_slices+(0,)] + A[A_first_slices+(0,1)]*B[B_first_slices+(1,)]
            AB[AB_first_slices+(1,)] = A[A_first_slices+(1,0)]*B[B_first_slices+(0,)] + A[A_first_slices+(1,1)]*B[B_first_slices+(1,)]
    else:
        A_first_slices = (np.s_[:],) * (A.ndim-1)
        if B.shape[-2:] == (2,2):
            # a_fn.T (1x2) x B_fn (2x2)
            B_first_slices = (np.s_[:],) * (B.ndim-2)
            if A.ndim-1 > B.ndim-2:
                AB = np.zeros(A.shape[:-1]+(2,), dtype=complex)
            else:
                AB = np.zeros(B.shape[:-2]+(2,), dtype=complex)
            AB_first_slices = (np.s_[:],) * (AB.ndim-1)
            AB[AB_first_slices+(0,)] = A[A_first_slices+(0,)]*B[B_first_slices+(0,0)] + A[A_first_slices+(1,)]*B[B_first_slices+(1,0)]
            AB[AB_first_slices+(1,)] = A[A_first_slices+(0,)]*B[B_first_slices+(0,1)] + A[A_first_slices+(1,)]*B[B_first_slices+(1,1)]
        else:
            # a_fn.T (1x2) x b_fn (2x1)
            B_first_slices = (np.s_[:],) * (B.ndim-1)
            AB = A[A_first_slices+(0,)]*B[B_first_slices+(0,)] + A[A_first_slices+(1,)]*B[B_first_slices+(1,)]
    return AB

def multi_det(A):
    """
    A: shape (...,2,2)
    """
    # For selecting all elements in the first 'A.ndim-2' dimensions of A
    first_slices = (np.s_[:],) * (A.ndim-2)

    return ( A[first_slices+(0,0)]*A[first_slices+(1,1)]
               - A[first_slices+(0,1)]*A[first_slices+(1,0)] )

def multi_trace(A):
    """
    A: shape (...,2,2)
    """
    # For selecting all elements in the first 'A.ndim-2' dimensions of A
    first_slices = (np.s_[:],) * (A.ndim-2)

    return A[first_slices+(0,0)] + A[first_slices+(1,1)]


def multi_inv(A):
    """
    A: shape (...,2,2)
    """
    invA = np.zeros(A.shape, dtype=complex)
    detA = multi_det(A) # shape (F,N)
    # For selecting all elements in the first 'A.ndim-2' dimensions of A
    first_slices = (np.s_[:],) * (A.ndim-2)
    invA[first_slices+(0,0)] = A[first_slices+(1,1)]
    invA[first_slices+(0,1)] = -A[first_slices+(0,1)]
    invA[first_slices+(1,0)] = invA[first_slices+(0,1)].conj()
    invA[first_slices+(1,1)] = A[first_slices+(0,0)]

    return invA/detA[first_slices+(np.newaxis,np.newaxis)]

def test():

    I = 2
    F = 513
    N = 100
    R = 10

    #%% Algebraic Riccati equation
    B = np.random.randn(I,I) + 1j*np.random.randn(I,I)
    B = (B + B.conj().T)/2 +  + 0.2*np.eye(I)
    A1 = np.random.randn(I,I) + 1j*np.random.randn(I,I) + 0.1*np.eye(I)
    A = A1@A1.conj().T
    print('----Algebraic Riccati equation----')
    X = ARE_solver(A, B, False)
    error = np.sum(X@A@X - B)
    print('error:', error, '\n')

    #%% Multiplication
    A = np.random.randn(R,F,N,I,I) + 1j*np.random.randn(R,F,N,I,I)
    B = np.random.randn(R,F,N,I,I) + 1j*np.random.randn(R,F,N,I,I)
    print('----Multiplication (mat x mat)----')
    print('A shape %s - B shape %s\n' % (A.shape, B.shape))
    tic = time.time()
    AB_fast = multi_mult(A,B)
    toc = time.time()
    print('fast:', toc-tic)
    tic = time.time()
    AB_slow = np.zeros((R, F, N, I, I), dtype=complex)
    for r in np.arange(R):
        for f in np.arange(F):
            for n in np.arange(N):
                AB_slow[r,f,n,:,:] = A[r,f,n,:,:]@B[r,f,n,:,:]
    toc = time.time()
    print('slow:', toc-tic)
    error = np.sum(AB_fast-AB_slow)
    print('error:', error, '\n')

    #%% Multiplication
    A = np.random.randn(F,N,I,I) + 1j*np.random.randn(F,N,I,I)
    B = np.random.randn(R,F,N,I,I) + 1j*np.random.randn(R,F,N,I,I)
    print('----Multiplication (mat x mat)----')
    print('A shape %s - B shape %s\n' % (A.shape, B.shape))
    tic = time.time()
    AB_fast = multi_mult(A,B)
    toc = time.time()
    print('fast:', toc-tic)
    tic = time.time()
    AB_slow = np.zeros((R, F, N, I, I), dtype=complex)
    for r in np.arange(R):
        for f in np.arange(F):
            for n in np.arange(N):
                AB_slow[r,f,n,:,:] = A[f,n,:,:]@B[r,f,n,:,:]
    toc = time.time()
    print('slow:', toc-tic)
    error = np.sum(AB_fast-AB_slow)
    print('error:', error, '\n')

    #%% Multiplication
    A = np.random.randn(R,F,N,I,I) + 1j*np.random.randn(R,F,N,I,I)
    B = np.random.randn(F,N,I,I) + 1j*np.random.randn(F,N,I,I)
    print('----Multiplication (mat x mat)----')
    print('A shape %s - B shape %s\n' % (A.shape, B.shape))
    tic = time.time()
    AB_fast = multi_mult(A,B)
    toc = time.time()
    print('fast:', toc-tic)
    tic = time.time()
    AB_slow = np.zeros((R, F, N, I, I), dtype=complex)
    for r in np.arange(R):
        for f in np.arange(F):
            for n in np.arange(N):
                AB_slow[r,f,n,:,:] = A[r,f,n,:,:]@B[f,n,:,:]
    toc = time.time()
    print('slow:', toc-tic)
    error = np.sum(AB_fast-AB_slow)
    print('error:', error, '\n')

    #%% Multiplication
    A = np.random.randn(R,F,N,I,I) + 1j*np.random.randn(R,F,N,I,I)
    B = np.random.randn(R,F,N,I) + 1j*np.random.randn(R,F,N,I)
    print('----Multiplication (mat x vec)----')
    print('A shape %s - B shape %s\n' % (A.shape, B.shape))
    tic = time.time()
    AB_fast = multi_mult(A,B)
    toc = time.time()
    print('fast:', toc-tic)
    tic = time.time()
    AB_slow = np.zeros((R, F, N, I), dtype=complex)
    for r in np.arange(R):
        for f in np.arange(F):
            for n in np.arange(N):
                AB_slow[r,f,n,:] = A[r,f,n,:,:]@B[r,f,n,:]
    toc = time.time()
    print('slow:', toc-tic)
    error = np.sum(AB_fast-AB_slow)
    print('error:', error, '\n')

    #%% Multiplication
    A = np.random.randn(F,N,I,I) + 1j*np.random.randn(F,N,I,I)
    B = np.random.randn(R,F,N,I) + 1j*np.random.randn(R,F,N,I)
    print('----Multiplication (mat x vec)----')
    print('A shape %s - B shape %s\n' % (A.shape, B.shape))
    tic = time.time()
    AB_fast = multi_mult(A,B)
    toc = time.time()
    print('fast:', toc-tic)
    tic = time.time()
    AB_slow = np.zeros((R, F, N, I), dtype=complex)
    for r in np.arange(R):
        for f in np.arange(F):
            for n in np.arange(N):
                AB_slow[r,f,n,:] = A[f,n,:,:]@B[r,f,n,:]
    toc = time.time()
    print('slow:', toc-tic)
    error = np.sum(AB_fast-AB_slow)
    print('error:', error, '\n')

    #%% Multiplication
    A = np.random.randn(R,F,N,I,I) + 1j*np.random.randn(R,F,N,I,I)
    B = np.random.randn(F,N,I) + 1j*np.random.randn(F,N,I)
    print('----Multiplication (mat x vec)----')
    print('A shape %s - B shape %s\n' % (A.shape, B.shape))
    tic = time.time()
    AB_fast = multi_mult(A,B)
    toc = time.time()
    print('fast:', toc-tic)
    tic = time.time()
    AB_slow = np.zeros((R, F, N, I), dtype=complex)
    for r in np.arange(R):
        for f in np.arange(F):
            for n in np.arange(N):
                AB_slow[r,f,n,:] = A[r,f,n,:,:]@B[f,n,:]
    toc = time.time()
    print('slow:', toc-tic)
    error = np.sum(AB_fast-AB_slow)
    print('error:', error, '\n')

    #%% Multiplication
    A = np.random.randn(R,F,N,I) + 1j*np.random.randn(R,F,N,I)
    B = np.random.randn(R,F,N,I,I) + 1j*np.random.randn(R,F,N,I,I)
    print('----Multiplication (vec x mat)----')
    print('A shape %s - B shape %s\n' % (A.shape, B.shape))
    tic = time.time()
    AB_fast = multi_mult(A, B)
    toc = time.time()
    print('fast:', toc-tic)
    tic = time.time()
    AB_slow = np.zeros((R, F, N, I), dtype=complex)
    for r in np.arange(R):
        for f in np.arange(F):
            for n in np.arange(N):
                AB_slow[r,f,n,:] = A[r,f,n,:]@B[r,f,n,:,:]
    toc = time.time()
    print('slow:', toc-tic)
    error = np.sum(AB_fast-AB_slow)
    print('error:', error, '\n')

    #%% Multiplication
    A = np.random.randn(F,N,I) + 1j*np.random.randn(F,N,I)
    B = np.random.randn(R,F,N,I,I) + 1j*np.random.randn(R,F,N,I,I)
    print('----Multiplication (vec x mat)----')
    print('A shape %s - B shape %s\n' % (A.shape, B.shape))
    tic = time.time()
    AB_fast = multi_mult(A, B)
    toc = time.time()
    print('fast:', toc-tic)
    tic = time.time()
    AB_slow = np.zeros((R, F, N, I), dtype=complex)
    for r in np.arange(R):
        for f in np.arange(F):
            for n in np.arange(N):
                AB_slow[r,f,n,:] = A[f,n,:]@B[r,f,n,:,:]
    toc = time.time()
    print('slow:', toc-tic)
    error = np.sum(AB_fast-AB_slow)
    print('error:', error, '\n')

    #%% Multiplication
    A = np.random.randn(R,F,N,I) + 1j*np.random.randn(R,F,N,I)
    B = np.random.randn(F,N,I,I) + 1j*np.random.randn(F,N,I,I)
    print('----Multiplication (vec x mat)----')
    print('A shape %s - B shape %s\n' % (A.shape, B.shape))
    tic = time.time()
    AB_fast = multi_mult(A, B)
    toc = time.time()
    print('fast:', toc-tic)
    tic = time.time()
    AB_slow = np.zeros((R, F, N, I), dtype=complex)
    for r in np.arange(R):
        for f in np.arange(F):
            for n in np.arange(N):
                AB_slow[r,f,n,:] = A[r,f,n,:]@B[f,n,:,:]
    toc = time.time()
    print('slow:', toc-tic)
    error = np.sum(AB_fast-AB_slow)
    print('error:', error, '\n')

    #%% Multiplication
    A = np.random.randn(R,F,N,I) + 1j*np.random.randn(R,F,N,I)
    B = np.random.randn(R,F,N,I) + 1j*np.random.randn(R,F,N,I)
    print('----Multiplication (vec x vec)----')
    print('A shape %s - B shape %s\n' % (A.shape, B.shape))
    tic = time.time()
    AB_fast = multi_mult(A,B)
    toc = time.time()
    print('fast:', toc-tic)
    tic = time.time()
    AB_slow = np.zeros((R, F, N), dtype=complex)
    for r in np.arange(R):
        for f in np.arange(F):
            for n in np.arange(N):
                AB_slow[r,f,n] = A[r,f,n,:]@B[r,f,n,:]
    toc = time.time()
    print('slow:', toc-tic)
    error = np.sum(AB_fast-AB_slow)
    print('error:', error, '\n')

    #%% Multiplication
    A = np.random.randn(F,N,I) + 1j*np.random.randn(F,N,I)
    B = np.random.randn(R,F,N,I) + 1j*np.random.randn(R,F,N,I)
    print('----Multiplication (vec x vec)----')
    print('A shape %s - B shape %s\n' % (A.shape, B.shape))
    tic = time.time()
    AB_fast = multi_mult(A,B)
    toc = time.time()
    print('fast:', toc-tic)
    tic = time.time()
    AB_slow = np.zeros((R, F, N), dtype=complex)
    for r in np.arange(R):
        for f in np.arange(F):
            for n in np.arange(N):
                AB_slow[r,f,n] = A[f,n,:]@B[r,f,n,:]
    toc = time.time()
    print('slow:', toc-tic)
    error = np.sum(AB_fast-AB_slow)
    print('error:', error, '\n')

    #%% Multiplication
    A = np.random.randn(R,F,N,I) + 1j*np.random.randn(R,F,N,I)
    B = np.random.randn(F,N,I) + 1j*np.random.randn(F,N,I)
    print('----Multiplication (vec x vec)----')
    print('A shape %s - B shape %s\n' % (A.shape, B.shape))
    tic = time.time()
    AB_fast = multi_mult(A,B)
    toc = time.time()
    print('fast:', toc-tic)
    tic = time.time()
    AB_slow = np.zeros((R, F, N), dtype=complex)
    for r in np.arange(R):
        for f in np.arange(F):
            for n in np.arange(N):
                AB_slow[r,f,n] = A[r,f,n,:]@B[f,n,:]
    toc = time.time()
    print('slow:', toc-tic)
    error = np.sum(AB_fast-AB_slow)
    print('error:', error, '\n')

    #%% norm 2
    A = np.random.randn(R,F,N,I) + 1j*np.random.randn(R,F,N,I)
    print('----Norm----')
    tic = time.time()
    AA_fast = multi_mult(A.conj(),A)
    toc = time.time()
    print('fast:', toc-tic)
    tic = time.time()
    AA_slow = np.sum(np.abs(A)**2, axis=-1)
    toc = time.time()
    print('slow:', toc-tic)
    error = np.sum(AA_fast-AA_slow)
    print('error:', error, '\n')

    #%% Determinant
    A = np.random.randn(R,F,N,I,I) + 1j*np.random.randn(R,F,N,I,I)
    print('----Determinant----')
    tic = time.time()
    detA_fast = multi_det(A)
    toc = time.time()
    print('fast:', toc-tic)
    tic = time.time()
    detA_slow = np.linalg.det(A)
    toc = time.time()
    print('slow:', toc-tic)
    error = np.sum(detA_fast - detA_slow)
    print('error:', error, '\n')

    #%% Trace
    A = np.random.randn(R,F,N,I,I) + 1j*np.random.randn(R,F,N,I,I)
    print('----Trace----')
    tic = time.time()
    trA_fast = multi_trace(A)
    toc = time.time()
    print('fast:', toc-tic)
    tic = time.time()
    trA_slow = np.trace(A, axis1=-2, axis2=-1)
    toc = time.time()
    print('slow:',toc-tic)
    error = np.sum(trA_fast - trA_slow)
    print('error:', error, '\n')

    #%% Inverse
    A = np.random.randn(R,F,N,I,I) + 1j*np.random.randn(R,F,N,I,I)
    A = ( (A + A.conj().transpose((0, 1, 2, 4, 3)))/2
         + 0.2*np.eye(I)[np.newaxis, np.newaxis, np.newaxis, :, :] )
    print('----Inverse----')
    tic = time.time()
    invA_fast = multi_inv(A)
    toc = time.time()
    print('fast:', toc-tic)
    tic = time.time()
    invA_slow = np.linalg.inv(A)
    toc = time.time()
    print('slow:',toc-tic)
    error = np.sum(invA_fast - invA_slow)
    print('error:', error, '\n')
